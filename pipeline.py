#!/usr/bin/env python3


import subprocess
import yaml
import argparse
import sys

parser = argparse.ArgumentParser(description='Scheduler')
parser.add_argument('-f', '--file', type=str, default='graph.yml',
                    help='graph file to load. reads graph.yml by default')
parser.add_argument('-t', '--target', type=str, default='',
                    help='target function to run. runs entire directed graph by default.')

args = parser.parse_args()

try:
    with open(args.file) as gf:
        graph = yaml.load(gf.read())
except EnvironmentError:
    print(f'unable to open file {args.file}')
    sys.exit(1)


def generate_run_list(graph={}):
    dependencies = []
    for key in graph.keys():
        # The first function will not have any dependcies.
        for dep in graph[key]['deps']:
            # We don't want to add any dependencies if they already exist.
            # Note, this prevents duplicate lists from being added, but does
            # not prevent situations like this:
            # [['A'], ['B'], ['A', 'C']...
            # See the run() function on how this is dealt with.
            if not any(dep in d for d in dependencies):
                dependencies.append(graph[key]["deps"])

    # Add the last function to the list.
    dependencies.append([sorted(list(graph.keys()))[-1]])
    return dependencies


def run(graph={}, target=''):
    run_list = generate_run_list(graph)

    if target == '':
        target = str(run_list[-1]).upper()

    count = 0
    processes_ran = []

    try:
        for step in run_list:
            processes = set()
            count = count + 1
            print(f'Step {count}')
            for func in step:
                if func not in processes_ran:
                    cmd = graph[func]["cmd"]
                    processes.add(subprocess.Popen([cmd], shell=True))
                    processes_ran.append(func)
                if target in func:
                    raise Exception
            for p in processes:
                p.wait()
    except:
        for p in processes:
            p.wait()


if __name__ == '__main__':
    run(graph, args.target)
