# Scheduler


## About
This small Python appliction is meant to take an arbitrary set of requirements from a YAML file that describes a dependency chain, and the functions neeeded to run in each step.

The application accepts an input file (reading graph.yml by default) and a target function (last function in the graph by default), and generates a pipeline, running functions in parallel if able.

##
Steps to run:
1. Ensure virtualenv is installed: `pip3 install virtualenv`
2. Clone the repository, and navigate to the directory.
3. Run `virtualenv env && source env/bin/activate`
4. Run `pip3 install -r requirements.txt`
5. Run `./pipeline.py`. Alternatively, use `-h` flag to see options.

## Examples
Example runs:
```
(env) pipeline (wip)$ ./pipeline.py -h
usage: pipeline.py [-h] [-f FILE] [-t TARGET]

Scheduler

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  graph file to load. reads graph.yml by default
  -t TARGET, --target TARGET
                        target function to run. runs entire directed graph by
                        default.
```

```
(env) pipeline (wip)$ ./pipeline.py
Step 1
A
Step 2
B
C
Step 3
E
D
Step 4
F
```

Specifying a file:

```
(env) pipeline (wip)$ ./pipeline.py -f graph2.yml
Step 1
A
Step 2
B
Step 3
C
Step 4
E
D
Step 5
F
(env) pipeline (wip)$
```

Specififying the target function:

```
(env) pipeline (wip)$ ./pipeline.py -f graph2.yml -t C
Step 1
A
Step 2
B
Step 3
C
(env) pipeline (wip)$
```